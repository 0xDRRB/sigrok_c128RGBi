# sigrokC128RGBi

This is a work-in-progress and POC showing how to use a FX2 logic analyzer with [libsigrok](https://sigrok.org/wiki/Libsigrok) to get an image from Commodore 128 RGB port.

This code is **absolutely not designed for production use**, or for anything other than experimentation.

"_Please excuse the crudity of this code. I didn't have time to build it to scale or paint it._"
