TARGET  := sigtest
WARN    := -Wall
DEBUG	:= -g
CFLAGS  := -O2 $(DEBUG) ${WARN} `pkg-config --cflags libsigrok`
LDFLAGS := `pkg-config --libs libsigrok`
CC      := gcc

C_SRCS    = $(wildcard *.c)
OBJ_FILES = $(C_SRCS:.c=.o)

all: ${TARGET}

%.o: %.c
	${CC} ${WARN} -c ${CFLAGS}  $< -o $@

${TARGET}: ${OBJ_FILES}
	${CC} ${WARN} -o $@  $(OBJ_FILES) ${LDFLAGS}

clean:
	rm -rf *.o ${TARGET}

mrproper: clean
	rm -rf *~

leaktest: ${TARGET}
	valgrind --leak-check=yes --show-leak-kinds=definite ./${TARGET}
