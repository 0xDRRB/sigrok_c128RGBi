#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <assert.h>
#include <libsigrok/libsigrok.h>

#define UNUSED(x) (void)(x)
#define SAMPLES 500000

#define WBUF    1100
#define HBUF     350
#define HADJUST  200

// exemple pourri (vieux)
// https://www.arthy.org/blog/an_logic/

// https://sigrok.org/api/libsigrok/0.5.2/index.html
// https://docs.gtk.org/glib/struct.Variant.html

uint64_t sample_counter = 0;

uint8_t vprev = 1;
uint8_t hprev = 0;

uint16_t hsync_cnt = 0;

typedef struct {
	uint8_t r;
	uint8_t g;
	uint8_t b;
} rgbfb;


rgbfb fbuffer[HBUF][WBUF];
int yy = 0;
int xx = 0;

int rec = 0;
int first = 1;

void foo(const struct sr_dev_inst *sdi, const struct sr_datafeed_packet *packet, void *cb_data)
{
	const struct sr_datafeed_logic *logic;
	uint8_t *data;
	FILE* file;

	switch(packet->type) {
		case SR_DF_HEADER:
			printf("SR_DF_HEADER\n"); break;
		case SR_DF_META:
			printf("SR_DF_META\n"); break;
		case SR_DF_TRIGGER:
			printf("SR_DF_TRIGGER\n"); break;
		case SR_DF_LOGIC:
			logic = packet->payload;
			sample_counter += logic->length / logic->unitsize;
			data = logic->data;

			for(uint64_t i=0; i<logic->length; i++) {
				// hsync
				if((data[i] & 2) != hprev && (data[i] & 2) == 0 && (data[i] & 1) == 0) {
					hsync_cnt++;
					// printf("%" PRIu64 ": H-tick %s  -> yy:%d\n", sample_counter - logic->length + i, data[i] & 2 ? "HIGH": "LOW", yy);
					yy++;
					xx = 0;
				}
				if(rec) {
					if(xx>HADJUST && xx<(WBUF-1+HADJUST)) {
						uint8_t intensity = data[i] & 32;
						fbuffer[yy][xx-HADJUST].b = data[i] &  4 ? (0xcd+intensity) : 0x00;
						fbuffer[yy][xx-HADJUST].g = data[i] &  8 ? (0xcd+intensity) : 0x00;
						fbuffer[yy][xx-HADJUST].r = data[i] & 16 ? (0xcd+intensity) : 0x00;
					}
					xx++;
				}
				// vsync
				if((data[i] & 1) != vprev) {
					//printf(">> %" PRIu64 ": V-tick %s\n", sample_counter - logic->length + i, data[i] & 1 ? "HIGH": "LOW");
					if((data[i] & 1) == 0 && first == 1) {
						printf("--- START REC ---\n");
						rec = 1;
						first = 0;
					}
					if((data[i] & 1) == 1) {
						printf("--- STOP REC ---\n");
						printf("%u hsync_cnt\n", hsync_cnt);
						hsync_cnt=0;
						rec = 0;
					}
				}
				vprev = data[i] & 1;
				hprev = data[i] & 2;
			}
			break;
		case SR_DF_FRAME_BEGIN:
			printf("SR_DF_FRAME_BEGIN\n"); break;
		case SR_DF_FRAME_END:
			printf("SR_DF_FRAME_END\n"); break;
		case SR_DF_ANALOG:
			printf("SR_DF_ANALOG\n"); break;
		case SR_DF_END:
			printf("SR_DF_END\n");
			printf("got %" PRIu64 " samples\n", sample_counter);

			// convert -size 1100x350 -depth 8 rgb:sig.data png:- | display
			// convert -size 1100x350 -depth 8 gray:nbsig.data png:- | display
			file = fopen("sig.data", "wb");
			fwrite(fbuffer, WBUF*HBUF*3, 1, file);
			fclose(file);

			break;
		default:
			printf("Unknown packet type\n");
	}
}

int main(int argc, char**argv)
{
	struct sr_dev_driver **driverlist;
	struct sr_context *ctx;
	struct sr_dev_driver *driver = NULL;
	GSList *devlist;
	struct sr_dev_inst *sdi = NULL;
	struct sr_session *ses;
	uint64_t mysrate, myslimit;
	GVariant *srate, *slimit, *newsr, *newsl;
	struct sr_trigger* mytrig;
	struct sr_trigger_stage* mytrigstage;


	/*
	struct sr_channel chan_hsync = {
		.sdi = sdi,
		.index = 1,
		.type = SR_CHANNEL_LOGIC,
		.enabled = true,
		.name = "hsync"
	};
	UNUSED(chan_hsync);

	struct sr_channel chan_blue = {
		.sdi = sdi,
		.index = 2,
		.type = SR_CHANNEL_LOGIC,
		.enabled = true,
		.name = "blue"
	};
	UNUSED(chan_blue);

	struct sr_channel chan_green = {
		.sdi = sdi,
		.index = 3,
		.type = SR_CHANNEL_LOGIC,
		.enabled = true,
		.name = "green"
	};
	UNUSED(chan_green);

	struct sr_channel chan_red = {
		.sdi = sdi,
		.index = 4,
		.type = SR_CHANNEL_LOGIC,
		.enabled = true,
		.name = "red"
	};
	UNUSED(chan_red);

	struct sr_channel chan_intens = {
		.sdi = sdi,
		.index = 5,
		.type = SR_CHANNEL_LOGIC,
		.enabled = true,
		.name = "intens"
	};
	UNUSED(chan_intens);
	*/

	if(sr_init(&ctx) != SR_OK) {
		fprintf(stderr, "Error initializing libsigrok!\n");
		exit(EXIT_FAILURE);
	}

	// Select 'fx2lafw' driver
	driverlist = sr_driver_list(ctx);
	for(int i=0; driverlist[i]; i++) {
		if(strcmp(driverlist[i]->name, "fx2lafw") == 0) {
			driver=driverlist[i];
			break;
		}
	}

	if(!driver) {
		fprintf(stderr, "Unable to select fx2lafw driver\n");
		exit(EXIT_FAILURE);
	}

	// use this driver
	sr_driver_init(ctx, driver);

	// use the first device for fx2lafw driver
	devlist = sr_driver_scan(driver, NULL);
	if(devlist) {
		if((sdi = devlist->data) == NULL) {
			fprintf(stderr, "Unable to get fx2lafw device data!\n");
			exit(EXIT_FAILURE);
		}
	} else {
		fprintf(stderr, "No fx2lafw device found!\n");
		exit(EXIT_FAILURE);
	}
	g_slist_free(devlist);
	printf("Using: %s %s %s %s %s\n",
			sr_dev_inst_vendor_get(sdi), sr_dev_inst_model_get(sdi),
			sr_dev_inst_version_get(sdi), sr_dev_inst_sernum_get(sdi),
			sr_dev_inst_connid_get(sdi));

	sr_dev_open(sdi);

	// Get config
	if(sr_config_get(driver, sdi, NULL, SR_CONF_SAMPLERATE, &srate) == SR_OK) {
		if(g_variant_is_of_type(srate, G_VARIANT_TYPE_UINT64)) {
			printf("Default sample rate = %lu\n", g_variant_get_uint64(srate));
			g_variant_unref(srate);
		}
	} else {
		fprintf(stderr, "Error getting SR_CONF_SAMPLERATE!\n");
	}

	if(sr_config_get(driver, sdi, NULL, SR_CONF_LIMIT_SAMPLES, &slimit) == SR_OK) {
		if(g_variant_is_of_type(slimit, G_VARIANT_TYPE_UINT64)) {
			printf("Default sample limite = %lu\n", g_variant_get_uint64(slimit));
			g_variant_unref(slimit);
		}
	} else {
		fprintf(stderr, "Error getting SR_CONF_LIMIT_SAMPLES!\n");
	}

	// Set config
	// sample rate (0 kHz, 25 kHz, 50 kHz, 100 kHz, 200 kHz, 250 kHz, 500 kHz, 1 MHz, 2 MHz, 3 MHz, 4 MHz, 6 MHz, 8 MHz, 12 MHz, 16 MHz, 24 MHz...)
	if(sr_parse_sizestring("24 MHz", &mysrate) == SR_OK) {
		newsr = g_variant_new_uint64(mysrate); // its refcount will be sunk and unreferenced after use
		if(sr_config_set(sdi, NULL, SR_CONF_SAMPLERATE, newsr) != SR_OK) {
			fprintf(stderr, "Error setting SR_CONF_SAMPLERATE!\n");
			exit(EXIT_FAILURE);
		}
		printf("New sample rate = %" PRIu64 "\n", mysrate);
	}

	// sample limit
	myslimit = SAMPLES;
	newsl = g_variant_new_uint64(myslimit);
	if(sr_config_set(sdi, NULL, SR_CONF_LIMIT_SAMPLES, newsl) != SR_OK) {
		fprintf(stderr, "Error setting SR_CONF_LIMIT_SAMPLES!\n");
		exit(EXIT_FAILURE);
	}
	printf("New sample limit = %" PRIu64 "\n", myslimit);

	// Apply config to hardware
	if(sr_config_commit(sdi) != SR_OK) {
		fprintf(stderr, "Error setting new configuration!\n");
		exit(EXIT_FAILURE);
	}

	struct sr_channel chan_vsync = {
		.sdi = sdi,
		.index = 0,
		.type = SR_CHANNEL_LOGIC,
		.enabled = true,
		.name = "vsync"
	};

	// new trigger
	mytrig = sr_trigger_new("trig on hsync");

	// add stage to trigger
	if((mytrigstage = sr_trigger_stage_add(mytrig)) == NULL) {
		fprintf(stderr, "Invalid trigger!\n");
		exit(EXIT_FAILURE);
	}

	// Add match to stage
	// SR_TRIGGER_RISING, SR_TRIGGER_FALLING
	if(sr_trigger_match_add(mytrigstage, &chan_vsync, SR_TRIGGER_RISING, 0.0f) != SR_OK) {
		fprintf(stderr, "Unable to configure trigger!\n");
		exit(EXIT_FAILURE);
	}

	sr_session_new(ctx, &ses);
	sr_session_datafeed_callback_add(ses, foo, NULL);
	sr_session_dev_add(ses, sdi);

	if(sr_session_trigger_set(ses, mytrig) != SR_OK) {
		fprintf(stderr, "Unable to add trigger!\n");
		exit(EXIT_FAILURE);
	}

	if(chan_vsync.sdi == NULL) {
		fprintf(stderr, "Et merde !\n");
		exit(EXIT_FAILURE);
	}

	sr_session_start(ses);
	sr_session_run(ses);

	sr_trigger_free(mytrig);
	sr_session_destroy(ses);
	sr_dev_close(sdi);

	sr_exit(ctx);

	return(EXIT_SUCCESS);
}
